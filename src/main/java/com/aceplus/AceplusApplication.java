package com.aceplus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AceplusApplication {

    public static void main(String[] args) {
        SpringApplication.run(AceplusApplication.class, args);
    }

}
