package com.aceplus.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Player {

    private String name;

    private int score;

    public Player(){}
}
