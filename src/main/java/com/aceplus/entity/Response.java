package com.aceplus.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Response<T> implements Serializable {

    private static final long serialVersionUID = 8992463810222512826L;

    private List<T> data;

    private int count;

    public Response() {

    }

    public Response(List<T> data) {
        setData(data);
    }

    public Response(List<T> data, int count) {
        setData(data);
        setCount(count);
    }
}
