package com.aceplus.entity;

import java.util.Comparator;

public class Checker implements Comparator<Player> {

    public int compare(Player a, Player b) {
        if (a.getScore() < b.getScore())
            return 1;
        else if (a.getScore() > b.getScore())
            return -1;
        else {
            int minLength = Math.min(a.getName().length(), b.getName().length());
            for (int i = 0; i < minLength; i++) {
                int aChar = Character.getNumericValue(a.getName().charAt(i));
                int bChar = Character.getNumericValue(b.getName().charAt(i));
                if (aChar < bChar)
                    return -1;
                else if (aChar > bChar)
                    return 1;
                else
                    continue;
            }

            if (a.getName().length() < b.getName().length())
                return -1;
            else if (a.getName().length() > b.getName().length())
                return 1;
            else
                return 0;
        }
    }
}
