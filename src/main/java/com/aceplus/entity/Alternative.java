package com.aceplus.entity;

public class Alternative {

    public static int getDeletionCount(String request) {
        int count = 0;
        for (int i = 0; i < request.length() - 1; ++i) {
            if (request.charAt(i) == request.charAt(i + 1)) {
                count++;
            }
        }
        return count;
    }
}
