package com.aceplus.entity;

import java.util.ArrayList;
import java.util.List;

public class Palindrome {

    public static List<String> findPalindromesInSubString(String input, int j, int k) {
        List<String> subStringList = new ArrayList<String>();
        for (; j >= 0 && k < input.length(); --j, ++k) {
            if (input.charAt(j) != input.charAt(k)) {
                break;
            }
            subStringList.add(input.substring(j, k + 1));
        }
        return subStringList;
    }


    public static List<String> findAllPalindromeSubstrings(String input) {
        List<String> subStringList = new ArrayList<String>();
        for (int i = 0; i < input.length(); ++i) {
            subStringList.add(input.substring(i, i + 1));
            subStringList.addAll(findPalindromesInSubString(input, i - 1, i + 1));
            subStringList.addAll(findPalindromesInSubString(input, i, i + 1));
        }
        return subStringList;
    }
}
