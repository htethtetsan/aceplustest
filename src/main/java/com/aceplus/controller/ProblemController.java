package com.aceplus.controller;

import com.aceplus.dto.PlayerDto;
import com.aceplus.entity.Alternative;
import com.aceplus.entity.Checker;
import com.aceplus.entity.Palindrome;
import com.aceplus.entity.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RestController
@Slf4j
@CrossOrigin(origins = "*")
@RequestMapping("problem")
public class ProblemController {

    @GetMapping("/one")
    public Response getPalindromeSubStrings(@RequestParam String request) {
        List<String> substringList = Palindrome.findAllPalindromeSubstrings(request);
        return new Response(substringList, substringList.size());
    }

    @GetMapping("/two")
    public int deleteAdjacentCharacters(@RequestParam String request) {
        return Alternative.getDeletionCount(request);
    }

    @PostMapping("/three")
    public Response sortPlayers(@RequestBody PlayerDto dto) {
        Collections.sort(dto.getPlayerList(), new Checker());
        return new Response(dto.getPlayerList());
    }

    public static String middle(String str) {
        int position = str.length() / 2 - 2;
        return str.substring(position, position + 5);
    }

    @GetMapping("/four")
    public String sort(@RequestParam String request) {
        request = request.replaceAll("[^a-zA-Z0-9]+", " ").replaceAll(" ", "");
        String middle = middle(request);
        char charArray[] = middle.toCharArray();
        Arrays.sort(charArray);
        return new String(charArray);
    }

    private String removeRepeatedCharacters(String word) {
        for (int i = 0; i < word.length() - 1; i++) {
            if (word.charAt(i) == word.charAt(i + 1))
                return word.replace(word.charAt(i) + "" + word.charAt(i + 1), word.charAt(i) + "");
        }
        return word;
    }

    @GetMapping("/five")
    public String reverse(@RequestParam String request) {
        String[] words = request.split(" ");
        String reverse = "";
        for (int i = words.length - 1; i >= 0; i--)
            reverse += removeRepeatedCharacters(words[i]) + " ";
        return reverse;
    }
}
