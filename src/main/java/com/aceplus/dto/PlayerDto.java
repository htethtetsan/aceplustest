package com.aceplus.dto;

import com.aceplus.entity.Player;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PlayerDto {

    private List<Player> playerList;
}
